class Employee < ApplicationRecord
  belongs_to :manager

  def full_name
    "#{first_name} #{last_name}"
  end
end
